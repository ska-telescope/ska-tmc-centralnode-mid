# -*- coding: utf-8 -*-
#
# This file is part of the CentralNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE for more info.

"""Release information for Python Package"""

name = """ska-tmc-centralnode"""
version = "0.18.1"
version_info = version.split(".")
description = """Central Node is a coordinator of the complete M&C system."""
author = "Team Sahyadri, Team Himalaya"
author_email = "telmgt-internal@googlegroups.com"
url = """https://www.skao.int/"""
