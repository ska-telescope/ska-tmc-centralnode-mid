# CentralNode

This project is developing the CentralNode component of Telescope Monitoring and Control (TMC) system, for the Square Kilometre Array. Central Node is a coordinator of the complete M&C system.

Documentation
-------------

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-tmc-centralnode/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-tmc-centralnode/en/latest/)

The documentation for this project, including how to get started with it,can be found in the `docs` folder, and can be better browsed in the SKA development portal:

* [CentralNode documentation](https://developer.skatelescope.org/projects/ska-tmc-centralnode/en/latest/ "SKA Developer Portal: CentralNode documentation")


