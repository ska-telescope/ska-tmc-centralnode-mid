"""Test case for cm all faulty"""

import pytest
from ska_tmc_common import HelperBaseDevice
from ska_tmc_common.op_state_model import TMCOpStateModel

from ska_tmc_centralnode.manager.component_manager_low import (
    CNComponentManagerLow,
)
from ska_tmc_centralnode.model.input import InputParameterLow
from tests.settings import DEVICE_LIST_LOW, logger, set_devices_unresponsive


@pytest.fixture()
def devices_to_load():
    """Devices to load for command invocation"""
    return (
        {
            "class": HelperBaseDevice,
            "devices": [
                {"name": "a/b/c"},
            ],
        },
    )


@pytest.mark.SKA_low
def test_all_low_devices_faulty(tango_context):
    """Test all low devices faulty"""
    op_state_model = TMCOpStateModel(logger)
    cm = CNComponentManagerLow(
        op_state_model,
        _input_parameter=InputParameterLow(None),
        logger=logger,
    )
    cm.add_multiple_devices(DEVICE_LIST_LOW)
    set_devices_unresponsive(cm, DEVICE_LIST_LOW)

    logger.info(f"Component manager faulty devices{cm.checked_devices}")
    logger.info(f"Component total devices{cm.devices}")

    for devInfo in cm.devices:
        assert devInfo.unresponsive
