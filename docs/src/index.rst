==========================
Central Node documentation
==========================

This project is developing the Central Node (Mid and Low) component of the Telescope Monitoring and Control (TMC) prototype, for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   getting_started/getting_started
   
.. toctree::
   :maxdepth: 1
   :caption: Developer guide

   developer_guide/code_quality


.. toctree::
   :maxdepth: 1
   :caption: API

   api/index

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.rst

.. toctree::
   :maxdepth: 2
   :caption: Introduction

   README


Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
