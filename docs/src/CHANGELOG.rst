###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[0.16.1]
********
* Utilised ska-telmodel v1.17.0
* Updated Assignresources json(interface v.4.0)
* Removed custom validations and added telmodel validation for Assignresources and Releaseresources commands.
* Included base classes v.1.0.0 updates 

[0.15.2]
********
* Utilise ska-telmodel v1.17.0
* Update Assignresources json(interface v.4.0)
* Remove custom validations and added telmodel validation for Assignresources and Releaseresources commands.

[0.16.0]
* Updated base classes v1.0.0
* Updated control model v1.0.0
* Updated pytango v9.5.0
* Moved the loadDishVccCfg related methods from component manager to Mid component manager.

[0.15.1]
********
* Update Push event mechanism for isDishVccConfigSet , DishVccValidationStatus

[0.15.0]
********
* Update in kValue range from 1-2221 to 1-1177

[0.14.6]
************
* Update CentralNode to work with dish-lmc chart 3.0.0
* Fix the issue about dishes not getting added into monitoring
* Fix issues in the tests

[0.14.5]
************
* Update CentralNode to work with dish-lmc chart 3.0.0

[Main]
******
* Update the .readthedocs.yaml and pyproject.toml file to fix the RTD generation

[0.14.4]
************
* Implement changes for introduced dishMode and pointingState attribute on HelperDishLNDevice

[0.14.3]
************
* Update pytango v9.4.2
* Update ska-tango-base library v0.19.1
* Update ska-tango-base chart v0.4.8
* Update ska-tango-util chart v0.4.10
* Update pylint v3.1.0

[0.1.2]
************